package com.mycompany.lab03;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.mycompany.lab03.Lab03;
import static com.mycompany.lab03.Lab03.matrix;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXGameJUnitTest {
    
    public OXGameJUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    @Test
    public void testCheckWin_X_Vertical_0(){
        String matrix[][] = {{"X","2","3"},{"X","5","6"},{"X","8","9"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Vertical_1(){
        String matrix[][] = {{"1","X","3"},{"4","X","6"},{"7","X","9"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Vertical_2(){
        String matrix[][] = {{"1","2","X"},{"4","5","X"},{"7","8","X"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Diagonally_0(){
        String matrix[][] = {{"X","2","3"},{"4","X","6"},{"7","8","X"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Diagonally_1(){
        String matrix[][] = {{"1","2","X"},{"4","X","6"},{"X","8","9"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal_0(){
        String matrix[][] = {{"X","X","X"},{"4","5","6"},{"7","8","9"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal_1(){
        String matrix[][] = {{"1","2","3"},{"X","X","X"},{"7","8","9"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal_2(){
        String matrix[][] = {{"1","2","3"},{"4","5","6"},{"X","X","X"}};
        String currentPlayer = "X";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Vertical_0(){
        String matrix[][] = {{"O","2","3"},{"O","5","6"},{"O","8","9"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Vertical_1(){
        String matrix[][] = {{"1","O","3"},{"4","O","6"},{"7","O","9"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Vertical_2(){
        String matrix[][] = {{"1","2","O"},{"4","5","O"},{"7","8","O"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Diagonally_0(){
        String matrix[][] = {{"O","2","3"},{"4","O","6"},{"7","8","O"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Diagonally_1(){
        String matrix[][] = {{"1","2","O"},{"4","O","6"},{"O","8","9"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Horizontal_0(){
        String matrix[][] = {{"O","O","O"},{"4","5","6"},{"7","8","9"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Horizontal_1(){
        String matrix[][] = {{"1","2","3"},{"O","O","O"},{"7","8","9"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Horizontal_2(){
        String matrix[][] = {{"1","2","3"},{"4","5","6"},{"O","O","O"}};
        String currentPlayer = "O";
        boolean result = Lab03.checkWin(matrix,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDraw(){
        int count = 9;
        boolean result = Lab03.isDraw(count);
        assertEquals(true,result);
    }
}
