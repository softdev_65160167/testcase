/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
*/

package com.mycompany.lab03;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
public class Lab03 {
    static Set<Integer> chosenNumbers = new HashSet<>();
    static String[][] matrix = new String[3][3];
    static {
        matrix[0][0] = "1";
        matrix[0][1] = "2";
        matrix[0][2] = "3";
        matrix[1][0] = "4";
        matrix[1][1] = "5";
        matrix[1][2] = "6";
        matrix[2][0] = "7";
        matrix[2][1] = "8";
        matrix[2][2] = "9";
    }
    static int count = 0;
    static String currentPlayer= "X";
    static int num = 0;
    static boolean gameOver = false;
    static String playAgain;
    static void printWelcome() {
       System.out.println("Welcome to XO");
    }
    static void printTable(){
        for(int i = 0;i < 3;i++){
            for(int j = 0;j < 3;j++){
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println("");
        }     
    }
    static void printTurn(){
        System.out.println(currentPlayer+" Turn");
    }
    static void inputNumber(){
        System.out.print("Please input number :");
        Scanner kb = new Scanner(System.in);
        num = kb.nextInt();
        if(chosenNumbers.contains(num)) {
            printTable();
            printTurn();
            inputNumber();
        }
        chosenNumbers.add(num);
    }
    static void changePlayer(){
        if("X".equals(currentPlayer)){
            currentPlayer = "O";
        }else{
            currentPlayer = "X";
        }
    }
    static void changeNumtoX(){
        if(num == 1){
            matrix[0][0] = "X";
        } else if (num == 2) {
            matrix[0][1] = "X";
        } else if (num == 3) {
            matrix[0][2] = "X";
        } else if (num == 4) {
            matrix[1][0] = "X";
        } else if (num == 5) {
            matrix[1][1] = "X";
        } else if (num == 6) {
            matrix[1][2] = "X";
        } else if (num == 7) {
            matrix[2][0] = "X";
        } else if (num == 8) {
            matrix[2][1] = "X";
        } else if (num == 9) {
            matrix[2][2] = "X";
        }
    }
        static void changeNumtoO(){
        if(num == 1){
            matrix[0][0] = "O";
        } else if (num == 2) {
            matrix[0][1] = "O";
        } else if (num == 3) {
            matrix[0][2] = "O";
        } else if (num == 4) {
            matrix[1][0] = "O";
        } else if (num == 5) {
            matrix[1][1] = "O";
        } else if (num == 6) {
            matrix[1][2] = "O";
        } else if (num == 7) {
            matrix[2][0] = "O";
        } else if (num == 8) {
            matrix[2][1] = "O";
        } else if (num == 9) {
            matrix[2][2] = "O";
        }
    }
    static boolean checkWin(String[][] matrix,String currentPlayer){
        //X_horizontal
        if(currentPlayer.equals("X")){
            if(matrix[0][0] == "X" && matrix[0][1] == "X" && matrix[0][2] == "X"){
                return true;
            }else if(matrix[1][0] == "X" && matrix[1][1] == "X" && matrix[1][2] == "X"){
                return true;
            }else if(matrix[2][0] == "X" && matrix[2][1] == "X" && matrix[2][2] == "X"){
                return true;
            }
            //X_vertical
            else if(matrix[0][0] == "X" && matrix[1][0] == "X" && matrix[2][0] == "X"){
                return true;
            }else if(matrix[0][1] == "X" && matrix[1][1] == "X" && matrix[2][1] == "X"){
                return true;
            }else if(matrix[0][2] == "X" && matrix[1][2] == "X" && matrix[2][2] == "X"){
                return true;
            }
            //X_diagonally
            else if(matrix[0][0] == "X" && matrix[1][1] == "X" && matrix[2][2] == "X"){
                return true;
            }else if(matrix[0][2] == "X" && matrix[1][1] == "X" && matrix[2][0] == "X"){
                return true;
            }
            }
            //O_horizontal
            if(currentPlayer.equals("O")){
            if(matrix[0][0] == "O" && matrix[0][1] == "O" && matrix[0][2] == "O"){
                return true;
            }else if(matrix[1][0] == "O" && matrix[1][1] == "O" && matrix[1][2] == "O"){
                return true;
            }else if(matrix[2][0] == "O" && matrix[2][1] == "O" && matrix[2][2] == "O"){
                return true;
                //O_vertical
            }else if(matrix[0][0] == "O" && matrix[1][0] == "O" && matrix[2][0] == "O"){
                return true;
            }else if(matrix[0][1] == "O" && matrix[1][1] == "O" && matrix[2][1] == "O"){
                return true;
            }else if(matrix[0][2] == "O" && matrix[1][2] == "O" && matrix[2][2] == "O"){
                return true;
                //O_diagonally
            }else if(matrix[0][0] == "O" && matrix[1][1] == "O" && matrix[2][2] == "O"){
                return true;
            }else if(matrix[0][2] == "O" && matrix[1][1] == "O" && matrix[2][0] == "O"){
                return true;
            }
            }
            return false;
    }
    static void isWin(){
        System.out.println(currentPlayer + " is Winner!");
    }
    static boolean isDraw(int count){
        if(count == 9){
            System.out.println("This Game is Draw!!");
            return true;
        }
        return false;
    }
    static void resetGame() {
    chosenNumbers.clear();
    count = 0;
    currentPlayer = "X";
    gameOver = false;
    matrix[0][0] = "1";
    matrix[0][1] = "2";
    matrix[0][2] = "3";
    matrix[1][0] = "4";
    matrix[1][1] = "5";
    matrix[1][2] = "6";
    matrix[2][0] = "7";
    matrix[2][1] = "8";
    matrix[2][2] = "9";
    }
    static void changeXO(){
            if("X".equals(currentPlayer)){
                changeNumtoX();
            }else{
                changeNumtoO();
            }
    }
    static void startNewgame(){
        System.out.print("Do you want to play again? (Y/N): ");
        Scanner kb = new Scanner(System.in);
        playAgain = kb.nextLine();
    }
    public static void main(String[] args) {
        printWelcome();
        while(true){
            printTable();
            printTurn();
            inputNumber();
            changeXO();
            if(checkWin(matrix,currentPlayer) == true){
                printTable();
                isWin();
                gameOver = true;
            }
            count++;
            if(count == 9){
                printTable();
                isDraw(count);
                gameOver = true;
            }
            if (gameOver == true) {
                startNewgame();
                if (playAgain.equalsIgnoreCase("Y")) {
                    resetGame();
                    printWelcome();
                    continue;
            } else {
                break;
            }
            }
            changePlayer();
        }
    }
}
//finally